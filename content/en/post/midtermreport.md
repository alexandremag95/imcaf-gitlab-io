---
date: 2017-04-09T10:58:08-04:00
description: "Mid Term Report"
featured_image: '/UC_V_FundoClaro-negro.png'
tags: ["scene"]
title: "Mid Term Report"
---

## Introduction

With the Electrical revolution, the world gained a much more powerful and efficient way to create motion, the AC electrical motor even to this day all throughout the diverse industries one of the constants is the reliance on these powerful electrical motors, be it either for powering heavy machinery, pumping liquids or gasses, Conveyors belts, lifts, trains, etc. But as reliable as they are, they are still susceptible to breaking down due to failures of its bearings, windings, overheating, etc. Such failures can easily lead to significant operational downtime and thus loss of production efficiency and profit, regular maintenance can reduce the risk of failure but it doesn’t eliminate it, luckily most of these failures aren’t caused by instantaneous events, instead they are caused by deterioration overtime of various components. Thus by closely monitoring the motor it's possible to detect the deterioration before it leads to catastrophic failure.
 	The purpose of the MHM is to do the sensing and signal processing needed and later transmitting to any device with internet access the current  state of the electric motors in real time, so that an user who can, based on this, act in order to prevent damages from happening, or that in case they do happen, reduce the severity of them. Thanks to the reading being done through non-invasive sensors, it avoids security and reliability concerns, maintains safety and simplifies the installation of the monitoring system. This report describes the main tasks, what work has been done, the changes in relation to the original plan and also includes the work plan for the coming months.

## Summary 

![image alt text](https://imcaf.gitlab.io/imcaf-gitlab-io/UC_V_FundoClaro-negro.png)

Quick theory of operation, three current signals read by the current transformers are amplified and then sent simultaneously to a three channel ADC to be digitized.
In the Microprocessor, the signals are read from the ADC, and are then processed using ffts and integrals, to obtain the necessary variables to determine the state of the motor. The result is then sent to the cloud, accessible by the user anywhere with internet access. For the security minded we also support streaming to a local network computer, or direct readings on the LCD, keeping the data offline.
There are three main aspects to this project, the hardware, the signal processing in the µController and the end user interface which could be the LCD on the board, a site online, or even a program on the user’s computer.  According to the schedule that we had planned and mentioned in the previous report, we have successfully completed the hardware design (tasks 1 to 5), we are currently  working on the assembly, test and calibration of the board (task 6) (missing the LCD screen), we also began testing the wifi connection on a breadboard prototype (task 7) with good initial results.
 We are currently on schedule to deliver the rest of the tasks.
 
